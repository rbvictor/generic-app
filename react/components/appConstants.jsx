import keyMirror from 'keymirror';

const appConstants = keyMirror({
    CREATE: null,
    READ: null,
    UPDATE: null,
    DELETE: null
});

export default appConstants;