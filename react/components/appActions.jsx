import appConstants from './appConstants.jsx';
import appDispatcher from './appDispatcher.jsx';

const appActions = {

    create : (item) => {
        appDispatcher.handleViewAction({
            actionType: appConstants.CREATE,
            data: item
        })
    },
    read : (item) => {
        appDispatcher.handleViewAction({
            actionType: appConstants.READ,
            data: item
        })
    },
    update : (item, newItem) => {
        appDispatcher.handleViewAction({
            actionType: appConstants.UPDATE,
            data: {
                item: item,
                newItem: newItem
            }
        })
    },
    delete : (item) => {
        appDispatcher.handleViewAction({
            actionType: appConstants.DELETE,
            data: item
        })
    }
};

export default appActions;