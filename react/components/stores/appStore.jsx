import {EventEmitter} from 'events';
import appDispatcher from '../appDispatcher.jsx';
import appConstants from '../appConstants.jsx';

const CHANGE_EVENT = 'change';

// private attributes and methods

var _data = {};

function _create(){
}

function _read(){
}

function _update(){
}

function _delete(){
}

// class AppStore

class AppStore extends EventEmitter {
    constructor(){
        super();
        this.dispatcherToken = null;
    }

    emitChange(){
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback){
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }

    getData(){
        return _data;
    }

}

// linking Dispatcher

var appStore = new AppStore();

appStore.dispatcherToken = appDispatcher.register((action) => {

    switch (action.actionType) {

        case appConstants.CREATE:
            _create();
            break;

        case appConstants.READ:
            _read();
            break;

        case appConstants.UPDATE:
            _update();
            break;

        case appConstants.DELETE:
            _delete();
            break;

        default:
            return true;

    }
    appStore.emitChange();

    return true;
});

export default appStore;
