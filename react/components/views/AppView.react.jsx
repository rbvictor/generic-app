import React from 'react';
import appStore from '../stores/appStore.jsx';


function getAppState() {
    return {
      data: appStore.getData()
    };
}

class AppView extends React.Component{
    constructor(props){
        super(props);
        this.state = getAppState();
        this._onChange = this._onChange.bind(this);
    }

    _onChange() {
        this.setState(getAppState());
    }

    componentDidMount(){
        appStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        appStore.removeChangeListener(this._onChange);
    }

    render() {
        return (
            <div>
            </div>
        );
    }

}

export default AppView;