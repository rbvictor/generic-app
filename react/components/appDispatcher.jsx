import {Dispatcher} from 'flux';

class AppDispatcher extends Dispatcher {

    handleViewAction(action){
        this.dispatch({
            source: 'VIEW_ACTION',
            action: action
        });
    }

    handleAsyncAction(action){
        this.dispatch({
            source: 'ASYNC_ACTION',
            action: action
        });
    }

}

var appDispatcher = new AppDispatcher();

export default appDispatcher;

