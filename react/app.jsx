import React from 'react';
import AppView from './components/views/AppView.react.jsx';

React.render(
    <AppView />,
    document.body
);